const mongoose = require('mongoose');

const OrderSchema = new mongoose.Schema({
    userId: {
        type: String,
        ref: 'User',
        required: [true, 'userId is required']
    },
    products: [
        {
            productId: {
                type: String,
                ref: 'Product',
                required: [true, 'productId is required']
            },
            quantity: {
                type: Number,
                required: [true, 'quantity is required']
            },
            price: {
                type: Number,
                ref: 'Product',
                required: [true, 'price is required']
            }
        }
    ],
    totalAmount: {
        type: Number,
        required: [true, 'totalAmount is required']
    },
    purchasedOn: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Order', OrderSchema);