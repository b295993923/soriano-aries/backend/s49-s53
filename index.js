const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user');
const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order');


const app = express();

const port = 4000;


app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


mongoose.connect("mongodb+srv://ariessoriano:admin123@batch295.vbupz3d.mongodb.net/Ecommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('connected to MongoDB Atlas.'));

app.use('/users', userRoutes);

app.use('/products', productRoutes);

app.use("/orders", orderRoutes);




if(require.main === module) {
    app.listen(process.env.PORT || port, () => { console.log(`Server is now running in port ${process.env.PORT || port}.`)});
};


module.exports = app;