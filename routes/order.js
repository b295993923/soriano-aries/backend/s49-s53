const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

const router = express.Router();

// Route to create a new order
router.post("/purchase", verify, orderController.createOrder);


// Route to retrieve all orders (admin only)
router.get("/allOrder", verify, verifyAdmin, orderController.getAllOrders);

router.get('/userOrders', verify, orderController.getUserOrders);


module.exports = router;
