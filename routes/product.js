const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;


const router = express.Router();


// Create product by Admin only
router.post("/", verify, verifyAdmin, productController.createProduct);

// Route for getting all products
router.get("/all", productController.getAllProducts);


// Route for retrieving all the active products
router.get("/active", verify, productController.getActiveProducts);

// Route to retrieve single product
router.get('/:productId', productController.getSingleProduct);

// Update product information by Admin only
router.put("/:productId", productController.updateProduct);

// Route for archiving a product admin only
router.put('/:productId/archive', verify, verifyAdmin,productController.archiveProduct);

// Route for Activating a product admin only
router.put('/:productId/activate', verify, verifyAdmin,productController.activateProduct);

// Route for search price
router.post('/searchByPrice', productController.searchProductsByPriceRange);

router.post('/search', productController.searchProductsByName);




module.exports = router;