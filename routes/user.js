const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;


const router = express.Router();

// Route for checking if user email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

router.post("/login", userController.loginUser);

// Route to retrieve user details
router.get("/details", verify, userController.getProfile);

// Route to retrieve all orders User
router.get("/getOrders",verify, userController.getUserOrders);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);

// Route to set User as Admin
router.put('/updateUserAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);

// Route for resetting the password
router.put('/reset-password', verify, userController.resetPassword);


module.exports = router;