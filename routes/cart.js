const express = require('express');
const router = express.Router();
const auth = require('../auth');
const cartController = require('../controllers/cart')

const { verify, verifyAdmin } = auth;

router.post("/",verify, cartController.addProductToCart);