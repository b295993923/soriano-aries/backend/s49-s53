const express = require("express");
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const jwt = require('jsonwebtoken');
const Order = require("../models/Order");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		// The "find" metho
		// returns a record if a match is found.
		if(result.length > 0) {
			return true; //"Duplicate email found"
		} else {
			return false;
		}
	})
};

// Register a user.

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		username: reqBody.username,
        email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	}).catch(err => err)
};

module.exports.loginUser = (req, res) => {
	return User.findOne({username: req.body.username}).then(result => {
		console.log(result);
		if(result == null) {
			return res.send(false);
		}	else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(result)});
			} else {
				return res.send(false);
			}
		}
	}).catch(err => res.send(err));
};


// Get user details
module.exports.getProfile = (req, res ) => {
	return User.findById(req.user.id).then( result => {
		console.log(result)
		result.password = ''
		return res.send(result);
	}).catch(err => res.send(err))
};

  

// Function to get user orders
module.exports.getUserOrders = async (req, res) => {
	try {
	  const userId = req.user.id; 
  
	  
	  const orders = await Order.find({ userId: userId }).populate('products');
	  console.log(orders);
	  res.json(orders);
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ message: 'Internal Server Error' });
	}
  };

//   funtion to set user as admin
module.exports.updateUserAsAdmin = async (req, res) => {
	try {
	  const { userId } = req.body;
  
	  // Check if the provided user ID exists
	  const user = await User.findById(userId);
	  if (!user) {
		return res.status(404).json({ message: 'User not found' });
	  }
  
	  // Update the user as an admin
	  user.isAdmin = true;
	  await user.save();
  
	  return res.json({ message: 'User updated as admin successfully' });
	} catch (error) {
	  console.error(error);
	  return res.status(500).json({ message: 'Internal server error' });
	}
  };
  
  // Function to reset the password
module.exports.resetPassword = async (req, res) => {
	try {
	  const { newPassword } = req.body;
	  const { id } = req.user; // Extracting user ID from the authorization header
  
	  // Hashing the new password
	  const hashedPassword = await bcrypt.hash(newPassword, 10);
  
	  // Updating the user's password in the database
	  await User.findByIdAndUpdate(id, { password: hashedPassword });
  
	  // Sending a success response
	  res.status(200).json({ message: 'Password reset successfully' });
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ message: 'Internal server error' });
	}
  };

  module.exports.updateProfile = async (req, res) => {
	try {
	  // Get the user ID from the authenticated token
	  const userId = req.user.id;
  
	  // Retrieve the updated profile information from the request body
	  const { username, email } = req.body;
  
	  // Update the user's profile in the database
	  const updatedUser = await User.findByIdAndUpdate(
		userId,
		{ username, email },
		{ new: true }
	  );
  
	  res.json(updatedUser);
	} catch (error) {
	  console.error(error);
	  res.status(500).json({ message: 'Failed to update profile' });
	}
  };
  
  