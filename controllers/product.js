const Product = require('../models/Product');
const User = require('../models/User');


// Function for Creating products

module.exports.createProduct = async (req, res) => {
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })
    return newProduct.save().then((product, error) => {
        if (error) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    }).catch(err => res.send(err));
};

// Function for getting all products
module.exports.getAllProducts = async (req, res) => {
    return Product.find({}).then(result => {
        return res.send(result);
    })
    .catch(err => res.send(err));
};

// Function for retrieving all the active products
module.exports.getActiveProducts = async (req, res) => {
    return Product.find({isActive: true}).then(result => {
        return res.send(result);
    })
    .catch(err => res.send(err));
};


// Function for retrieving single product

module.exports.getSingleProduct = async (req, res) => {
 return Product.findById(req.params.productId).then(result => {
    console.log(result);
    return res.send(result);
 })
 .catch(err => res.send(err));
};

// Function for updating product for Admin only
module.exports.updateProduct = async (req, res) => {
    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
    console.log(updatedProduct);
    return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
        console.log(product)
        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err));
};


// Function for archiving a product Admin only
module.exports.archiveProduct = async (req, res) => {
    let archiveProduct = {
        isActive: false
    }
    return Product.findByIdAndUpdate(req.params.productId, archiveProduct).then((product, error) => {
        console.log(product)
        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    }).catch(err => res.send(err));
  };

//   Function for Activating a product Admin only
module.exports.activateProduct = async (req, res) => {
    let activateProduct = {
        isActive: true
    }
    return Product.findByIdAndUpdate(req.params.productId, activateProduct).then((product, error) => {
        console.log(product)
        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    }).catch(err => res.send(err));
};

// Function for search price
module.exports.searchProductsByPriceRange = async (req, res) => {
	const { minPrice, maxPrice } = req.body;
  
	try {
	  const products = await Product.find({
		price: { $gte: minPrice, $lte: maxPrice }
	  });
  
	  res.json(products);
	} catch (error) {
	  res.status(500).json({ error: 'Internal server error' });
	}
  };

  module.exports.searchProductsByName = async (req, res) => {
    try {
      const { productName } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const products = await Product.find({
        name: { $regex: productName, $options: 'i' }
      });
  
      res.json(products);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };
  